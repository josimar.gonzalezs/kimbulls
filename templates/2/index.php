<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="apple-touch-icon" sizes="57x57" href="images/icons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/icons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/icons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/icons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/icons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/icons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/icons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/icons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="images/icons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="images/icons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="images/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="images/icons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="images/icons/favicon-16x16.png">
	<link rel="manifest" href="images/icons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="images/icons/ms-icon-144x144.png">
    <title>KIMBULLS CAR ESTHETIC</title>
    <!-- Agregar los archivos CSS de Bootstrap 5 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <!-- Agregar estilos personalizados para el tema oscuro -->
    <style>
        body {
            background-color: #1e1e1e;
            color: #fff;
        }
        .navbar {
            background-color: #292929;
        }
        .navbar-brand {
            color: #fff;
        }
        .navbar-nav .nav-link {
            color: #fff;
        }
        .carousel-control-prev-icon,
        .carousel-control-next-icon {
            filter: invert(100%);
        }
        .navbar-text{
            font-size: 1.7rem;
        }
        a.nav-link.btn:hover {
            background-color: #f94e41;
        }
    </style>
</head>
<body>
    <!-- Agregar el menú de navegación -->
    <nav class="navbar navbar-expand-lg navbar-dark" id="inicio">
        <div class="container-fluid">
            <a class="navbar-brand" href=""><img width="100%" src="images/kimbulls_logo_web-removebg-preview.png"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link navbar-text btn btn-secondary" href="#inicio">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navbar-text btn btn-secondary" href="#quienes-somos">Quiénes somos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navbar-text btn btn-secondary" href="#servicios">Servicios</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navbar-text btn btn-secondary" href="#contacto">Contacto</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- <div class="container"> -->
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="images/imagen_1.jpg" class="img-fluid d-block w-100" alt="Imagen 1">
                </div>
                <div class="carousel-item">
                <img src="images/imagen_2.jpg" class="img-fluid d-block w-100" alt="Imagen 2">
                </div>
                <div class="carousel-item">
                <img src="images/imagen_3.jpg" class="img-fluid d-block w-100" alt="Imagen 3">
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    <!-- </div> -->
    <section id="quienes-somos" class="py-5">
        <div class="container">
            <div class="row">
            <div class="col">
                <h2 class="text-center mb-5">¿Quiénes somos?</h2>
                <p>Kimbulls es una empresa de estética de automóviles en Chile con varios años de experiencia. Nos especializamos en brindar servicios de limpieza y detallado de alta calidad para todo tipo de vehículos, desde automóviles de lujo hasta camiones comerciales.</p>
                <p>Nuestro equipo de expertos en estética automotriz está altamente capacitado y equipado con las últimas tecnologías y herramientas para garantizar que su vehículo reciba el mejor tratamiento posible. Nos enorgullecemos de ofrecer un servicio excepcional a nuestros clientes y de asegurarnos de que su automóvil se vea como nuevo después de cada visita.</p>
                <p>Si desea obtener más información sobre nuestros servicios o programar una cita para su vehículo, no dude en ponerse en contacto con nosotros. ¡Estamos aquí para ayudarlo a mantener su automóvil en excelentes condiciones y hacer que se vea increíble en todo momento!</p>
            </div>
            </div>
        </div>
    </section>
    <section>
        <h4 class="text-center" style="background: #f94e41;">Su vehículo es nuestra prioridad</h4>
    </section>
    <section id="servicios" class="py-5 bg-dark">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="text-center mb-5">Nuestros servicios</h2>
                    <div class="row">
                        <div class="col-md-4 mb-4">
                            <div class="card h-100 bg-dark">
                                <img src="images/servicio1.jpg" class="card-img-top" alt="Servicio 1">
                                <div class="card-body">
                                    <h5 class="card-title text-light">Servicio 1</h5>
                                    <p class="card-text text-light">Descripción del servicio 1.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-4">
                            <div class="card h-100 bg-dark">
                                <img src="images/servicio2.jpeg" class="card-img-top" alt="Servicio 2">
                                <div class="card-body">
                                    <h5 class="card-title text-light">Servicio 2</h5>
                                    <p class="card-text text-light">Descripción del servicio 2.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-4">
                            <div class="card h-100 bg-dark">
                                <img src="images/servicio3.jpg" class="card-img-top" alt="Servicio 3">
                                <div class="card-body">
                                    <h5 class="card-title text-light">Servicio 3</h5>
                                    <p class="card-text text-light">Descripción del servicio 3.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="contacto" class="py-5 bg-dark text-white">
        <div class="container">
            <div class="row">
            <div class="col">
                <h2 class="text-center mb-5">Contáctanos</h2>
                <form>
                <div class="form-group">
                    <label for="email">Correo electrónico:</label>
                    <input type="email" class="form-control" id="email" placeholder="ejemplo@ejemplo.com">
                </div>
                <div class="form-group">
                    <label for="message">Mensaje:</label>
                    <textarea class="form-control" id="message" rows="6"></textarea>
                </div>
                <button type="submit" class="btn btn-secondary">Enviar mensaje</button>
                </form>
            </div>
            </div>
        </div>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
</body>
<footer class="py-3" style="background: #f94e41;">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <p>Teléfono: <a href="tel:+22222222" class="text-white">(+562) 2222 222</a></p>
        <!-- <p>Dirección: <a href="https://www.google.com/maps?q=Bah%C3%ADa+Inglesa+1512,+La+Florida,+Regi%C3%B3n+Metropolitana,+Chile" class="text-white" target="_blank">Bahía Inglesa 1512 comuna de La Florida, Santiago, Región Metropolitana, Chile.</a></p> -->
      </div>
      <div class="col-md-6 text-md-end">
        <p>Desarrollado por <a href="https://www.develcode.cl" class="text-white" target="_blank">Develcode.cl</a></p>
      </div>
    </div>
  </div>
</footer>
</html>

<?php
    session_start();
    if(isset($_POST['email'])) {
        // Recibe los datos del formulario
        $name = $_POST['name'];
        $email = $_POST['email'];
        $message = $_POST['message'];
        
        // Configura los detalles del correo
        $destinatario = 'contacto@kimbulls.cl'; // Cambia esta dirección de correo por la tuya
        $asunto = 'Nuevo mensaje del formulario de contacto '.date("YmdHis");

        // Construye el cuerpo del correo
        $cuerpo = "Nombre: $name\n\n";
        $cuerpo .= "Email: $email\n\n";
        $cuerpo .= "Mensaje:\n$message\n";

        // Envía el correo
        $headers = "From: $name <$email>\r\n";
        $headers .= "Reply-To: $email\r\n";
        
        if(mail($destinatario, $asunto, $cuerpo, $headers)) {
            $_SESSION["validador_respuesta_correo"] = 1;
            $_SESSION["respuesta_correo"] = 'Mensaje enviado correctamente.';
        } else {
            $_SESSION["validador_respuesta_correo"] = 0;
            $_SESSION["respuesta_correo"] = 'Error al enviar el mensaje. Por favor, inténtalo de nuevo más tarde.';
        }
    }

    header("Location: ./#contacto");
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="apple-touch-icon" sizes="57x57" href="images/icons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/icons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/icons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/icons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/icons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/icons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/icons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/icons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="images/icons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="images/icons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="images/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="images/icons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="images/icons/favicon-16x16.png">
	<link rel="manifest" href="images/icons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="images/icons/ms-icon-144x144.png">
    <title>KIMBULLS CAR ESTHETYC</title>
    <!-- Agregar los archivos CSS de Bootstrap 5 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <!-- Agregar estilos personalizados para el tema oscuro -->
    <style>
        body {
        background-color: #1e1e1e;
        color: #fff;
        }
        .navbar {
        background-color: #292929;
        }
        .navbar-brand {
        color: #fff;
        }
        .navbar-nav .nav-link {
        color: #fff;
        }
        .carousel-control-prev-icon,
        .carousel-control-next-icon {
        filter: invert(100%);
        }
    </style>
</head>
<body>
    <!-- Agregar el menú de navegación -->
    <nav class="navbar navbar-expand-lg navbar-dark">
        <div class="container-fluid">
        <a class="navbar-brand" href=""><img src="images/kimbulls_logo_web-removebg-preview.png"></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="#">Inicio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Quiénes somos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Contacto</a>
            </li>
            </ul>
        </div>
        </div>
    </nav>
    <div id="carouselExample" class="carousel slide">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="images/imagen_1.jpg" class="d-block w-100" alt="Imagen 1">
            </div>
            <div class="carousel-item">
            <img src="images/imagen_2.jpg" class="d-block w-100" alt="Imagen 2">
            </div>
            <div class="carousel-item">
            <img src="images/imagen_3.jpg" class="d-block w-100" alt="Imagen 3">
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
</body>
</html>
